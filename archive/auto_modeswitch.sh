#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi


log "Starting auto_modeswitch.sh"
log "SWITCHED:($_SWITCHED)"
if [[ $_SWITCHED -ne 1 ]]; then
    #12d1 is the Huawei vendor code, yes this script assumes all modems are Huawei
    #Check if any Huawei modems are pluged in
    usb_devices=`lsusb`
    modem=`echo "$usb_devices" | grep -c 12d1`
    log "USB Device List:\n$usb_devices"
    #If we have found a modem
    if [ $modem -ge 1 ]; then
        #Identify the model
        model=`echo "$usb_devices" | grep 12d1 | sed -n -e 's/^.*12d1:\(....\).*/\1/p'`
        log "Model detected: $model"
        #1506 is a modem, know from experiance, may need to add more as we use more modems
        if [ "$model" != '1506' ]; then
            #We need to perform a usb mode switch.
            log "usb_modeswitch required"
            sudo usb_modeswitch -v12d1 -p15ca -M 55534243123456780000000000000011062000000100000000000000000000
        else 
            export _SWITCHED=1
            log "SWITCHED post script:($_SWITCHED)"
        fi
    else
	log "No modem present to swicth"
	export _SWITCHED=1
    fi
else
    log "usb_modeswitch not required"
fi
