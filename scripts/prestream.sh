#!/bin/bash
#This script is executed by liquidsoap on starting the steam to icecast
#In order to minimise inteference we switch off the wifi driver
#If no one is connected:

ap_users=$(iw wlan0 station dump | grep Station | wc -l)
if [[ "${ap_users}" -eq "0" ]] ; then
    sudo ifconfig wlan0 down
fi
