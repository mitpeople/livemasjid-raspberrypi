#!/bin/bash
#This script is executed by liquidsoap when the stream ends

#bring wifi back on if it was turned off
wifi_up=$(ifconfig | grep wlan0 | wc -l)
if [[ "${wifi_up}" -eq "0" ]] ; then
    sudo ifconfig wlan0 up
fi

#timestamp and recording files:
if [ -f "/home/pi/ram/dump.mp3" ]
then
        mv /home/pi/ram/dump.mp3 /home/pi/arecordings/$(date +%Y%m%d%H%M%S)_livedump.mp3
fi
if [ -f "/home/pi/ram/dump.ogg" ]
then
        mv /home/pi/ram/dump.ogg /home/pi/arecordings/$(date +%Y%m%d%H%M%S)_livedump.ogg
fi
