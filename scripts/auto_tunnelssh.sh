#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

#Set configuration variables
set -a; . ~/scripts/config.sh ; set +a

#Define config variables if not set
if [[ -z $tunnel_monitor ]]; then
        tunnel_monitor=2000
fi
if [[ -z $tunnel_port ]]; then
        tunnel_port=3000
fi
if [[ -z $tunnel_host ]]; then
        tunnel_host="livemasjid.com"
fi

if [[ "$1" == "stop" ]]; then
    log "STOP signal recieved, killing any tunnel parts using 'localhost:22.*${tunnel_host}'"
    pkill -9 -f "localhost:22.*${tunnel_host}";
    exit $?;
fi

log "Tunnel not up, check for connectivity"
ping -q -c3 $tunnel_host > /dev/null 2>&1
if [ $? -eq 0 ]; then
    pkill -9 -f "localhost:22.*${tunnel_host}"
    autossh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ExitOnForwardFailure=yes -o TCPKeepAlive=yes -o ServerAliveInterval=30 -o ServerAliveCountMax=5 -N -M $tunnel_monitor -R $tunnel_port:localhost:22 pi@$tunnel_host 2>&1 &
    log "Connected and able to ping ${tunnel_host}"
    setStatus "Internet" "0" "Connected and able to ping ${tunnel_host}" ;
else
    log "No Internet Connectivity - Cannot ping ${tunnel_host}"
    setStatus "Internet" "3" "No Internet Connectivity - Cannot ping ${tunnel_host}" ;
fi
