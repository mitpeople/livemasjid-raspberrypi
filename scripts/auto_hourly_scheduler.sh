#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
	function log () {
    		if [[ $_V -eq 1 ]]; then
        		echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
    		fi
	}
fi

minute=$(date "+%-M")

if [ "$minute" == "0" ]; then
    log "Hourly Scheduler Trigger Activated";
    /home/pi/scripts/auto_scheduler.sh &
else
    log "Not the top of the hour no need to waste time/resources testing the mic";
fi

