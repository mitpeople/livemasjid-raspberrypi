#!/bin/bash

#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

#This script will be used to expand the recordings partition to occupy the SD size

#check Max SD size
maxSDSize=`echo 'p' | sudo fdisk /dev/mmcblk0 | grep "Disk /dev/mmcblk0" | sed -n -e 's/.*, \([0-9]\+\) sectors/\1/p'`;
maxSDSize=`echo "$(($maxSDSize-1))"`;
#Get start and end sectors of last partition #todo, detect last partition
startPartionSector=`echo 'p' | sudo fdisk /dev/mmcblk0 | grep "/dev/mmcblk0p3" | tr -s ' ' | cut -d' ' -f2`;
endPartionSector=`echo 'p' | sudo fdisk /dev/mmcblk0 | grep "/dev/mmcblk0p3" | tr -s ' ' | cut -d' ' -f3`;

log "Checking if SD must be expanded, SD Card is $maxSDSize size and last partition currently at $endPartionSector";

if [ $maxSDSize -ne $endPartionSector ]; then
    log "Expand required as SD is not fully utilised";

    sudo umount /home/pi/arecordings;

    sudo e2fsck -y -f /dev/mmcblk0p3;

    #Method 1
    #echo -e "d\n3\nn\np\n3\n9232384\n31116287\nw" | sudo fdisk /dev/mmcblk0

    #Method 2
    (
    echo d # Delete partition 3 i.e. recordings partition
    echo 3 #
    echo n # Create new partition
    echo p # Primary partition to replace deleted 3
    echo 3 # Partition number
    echo $startPartionSector  # From old starting point
    echo $maxSDSize  # To Max size that SD allows
    echo w # Write changes
    ) | sudo fdisk /dev/mmcblk0;

    #partprobe informs the kernal of a partition change, may be useful in avoiding a reboot.
    sudo partprobe /dev/mmcblk0p3
    sudo partprobe /dev/mmcblk0
    sleep 10;

    sudo e2fsck -y -f /dev/mmcblk0p3;
    sudo resize2fs /dev/mmcblk0p3;
    sudo resize2fs /dev/mmcblk0p3;

    sudo mount /dev/mmcblk0p3 /home/pi/arecordings;
else
    log "Expand not required as maximum SD space is used."
fi
