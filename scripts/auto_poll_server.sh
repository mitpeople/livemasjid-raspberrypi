#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
	function log () {
    		if [[ $_V -eq 1 ]]; then
        		echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
    		fi
	}
fi

minute=$(date "+%-M")
second=$(date "+%-S")
if [ "$minute" == "0" ] && [ "$second" -lt "30" ]; then
    mount=$(hostname)
    #mount='northcliff'
    log "Polling livemasjid server for any remote management commands for '${mount}'";

    #cipher_cmd=`curl -s "http://livemasjid.com/api/get_commands.php?mount=${mount}"`
    #echo $cipher_cmd
    #sharedkey="l1vem@sjid"
    #key=`echo -n "${mount}${sharedkey}" | od -A n -t x1 | tr -d '\040\011\012\015'`
    #ivascii="abcdefghijklmnop"
    #iv=`echo -n "${ivascii}" | od -A n -t x1 | tr -d '\040\011\012\015'`
    #echo $key
    #echo $iv
    #echo -n $cipher_cmd | openssl aes-256-cbc -a -d -nosalt -nopad -K $key -iv $iv

    server_response=$(curl -s -w "%{http_code}\\n" "https://www.livemasjid.com/api/get_commands.php?mount=${mount}")
    pi_time=$(date +'%s')
    status_code=$(echo "$server_response" | tail -1)
    if [[ $status_code -eq 200 ]]; then
        commands=$(echo "${server_response}" | head -n -1)
        if [ -z "$commands" ]
        then
            log "No commands to run"
        else
	    log "Commands found, Will proceed to execute..."
            while read -r command; do
	        cmdid=0;
                #echo "... $command ..."
    	        command_time=$(echo $command | sed -n -e 's/\(.*\)|||\(.*\)|||\(.*\)/\1/p')
    	        command_id=$(echo $command | sed -n -e 's/\(.*\)|||\(.*\)|||\(.*\)/\2/p')
                command_name=$(echo $command | sed -n -e 's/\(.*\)|||\(.*\)|||\(.*\)/\3/p')
	        #echo "...... $command_time"
	        #echo "...... $command_name"
	        timedelta=$((pi_time-command_time))
	        #echo $timedelta
	        log "Command: ${command_name} sent at ${command_time} with command id: ${command_id}. Time delta $timedelta"
	        if [[ $timedelta -lt 5000 ]]; then
	            cmd_result=$(eval "$command_name");
	            log "Command: ${command_name} executed with result ${cmd_result}"
	            ## post result to server for recording
	            curl -X POST --data "cmdid=(${command_id})&mount=(${mount})&res=(${cmd_result})" https://www.livemasjid.com/api/command_response.php
                fi
            done <<< "$commands"
        fi
    else
	log "Server returned status code: ${status_code} and content: ${server_response}";
    fi
fi
