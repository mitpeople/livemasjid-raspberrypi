#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
	function log () {
    		if [[ $_V -eq 1 ]]; then
        		echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
    		fi
	}
fi

#Define config variables if not set
if [[ -z $enable_scheduler ]]; then
       enable_scheduler=1
fi

#Is Scheduler Enabled?
if [ "$enable_scheduler" == "1" ]; then
    log "Scheduler will check if streaming should be activated for this hour";
    #Get current day (numeric) and time (hour 24)
    day=$(date "+%u")
    hour=$(date "+%-H")

    #load scheduler configuration
    unset scheduler
    declare -A scheduler
    sday=1
    shour=0
    for daydata in `cat ~/scripts/scheduler.conf`
    do
	#echo "<$sday,$shour>=<$daydata>"
	scheduler[$sday,$shour]=$daydata
	if [ "$shour" -eq "23" ]; then
	    shour=0
	    sday=$((sday+1))
	else
	    shour=$((shour+1))
	fi
    done

    ##Debug to print current schedueler matrix
    #for ((j=1;j<=7;j++)) do
    #	for ((i=0;i<=23;i++)) do
    #        printf " %s" ${scheduler[$j,$i]}
    #	done
    #	echo
    #done

    #echo "According to the schedule for day:($day) and hour:($hour) streming is currently set to : ${scheduler[$day,$hour]}"

    mic_enabled=`amixer -c1 sget Mic | grep "Capture.\+dB] \[\w\+\]" | sed -n -e 's/.\+\[\(\(on\|off\)\)]/\1/p'`

    if [ "${scheduler[$day,$hour]}" == "1" ] && [ "$mic_enabled" == "on"  ]; then
	#switch off mic (muteing)
	log "Scheduler has mic set as off but mic is on... switching mic off";
	setStatus "Scheduler" "1" "Scheduler Running: Mic Turned Off";
	amixer -c1 set Mic nocap > /dev/null 2>&1
    elif [ "${scheduler[$day,$hour]}" == "0" ] && [ "$mic_enabled" == "off"  ]; then
	#switch on mic
	log "Scheduler has mic set as on but mic is off... switching mic on";
	setStatus "Scheduler" "1" "Scheduler Running: Mic Turned On";
	amixer -c1 set Mic cap > /dev/null 2>&1
    else
	log "No Mic state change required as mic is: $mic_enabled"
	setStatus "Scheduler" "0" "Scheduler Running";
    fi

else
    log "Scheduler disabled in global config";
    setStatus "Scheduler" "2" "Scheduler disabled in global config.";
fi
