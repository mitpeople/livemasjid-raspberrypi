#!/bin/bash
function log () {
    if [[ $_V -eq 1 ]]; then
        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
    fi
}
export -f log

function setStatus () {
	#Status allows us to record the high level state of required processes
	#A state can take the form
	#Name, <0(Green), 1(unknown), 2(Orange), 3(Red)> , Description (optional)
	#eg. Audio,0, Recording
	statuscheck=`cat /home/pi/ram/sysmonitor.data | grep -c "^${1}"`
	if [[ $statuscheck -le 0 ]]; then
		#echo "Status does not exist :${statuscheck}:"
		#if the status does not exit add the line
		if [ -n "$1" ] && [ -n "$2" ]; then
			printf "%s,%s" "$1" "$2" >> /home/pi/ram/sysmonitor.data
			if [ -n "$3" ]; then
				printf ",%s" "$3" >> /home/pi/ram/sysmonitor.data
			fi
			printf "\n" >> /home/pi/ram/sysmonitor.data
		fi
	else
		#if the status exists just update the line
		sed -i -e "s/^${1}.\+$/${1},${2},${3}/" /home/pi/ram/sysmonitor.data
		#echo "Status EXISTS"
	fi
}
export -f setStatus

export TERM=xterm

#Set configuration variables
set -a ; . ~/scripts/config.sh ; set +a

log "*****************************************"
log "Starting Up Raspberry Pi Streaming Server"
log "*****************************************"

/home/pi/scripts/auto_bootup_setup.sh
echo "0" > /home/pi/ram/liquidsoap_runcount.txt
/home/pi/scripts/auto_push_config.sh &

#Check services are up and running every 30seconds
while true; do
	log "processing scripts..."

	#auto_connect is currently only responsible for 3G USB modems, however it can be disabled in global config
	#Disabled sice V9 as connection is handled by Network Manager nmcli
        #/home/pi/scripts/auto_connect.sh &

	#Setup the reverse tunnel so that remote access is possible
	#Disabled since V9 as tunnel is now managed via monit
        #/home/pi/scripts/auto_tunnelssh.sh &

	#Only required if local audio storage is used
	/home/pi/scripts/auto_cleanfiles.sh &

    	#liquidsoap will stream the audio using config masjid.liq or masjid_opus.liq
	#Disabled since V9 as liquidsoap is now managed via monit
        #/home/pi/scripts/auto_stream_liquid.sh &

	#Last line backup that will continuosly record to disk
	/home/pi/scripts/auto_record.sh &

	#Automatic Scheduling Functionality
	/home/pi/scripts/auto_hourly_scheduler.sh &

	#Poll Server for any commands to run
	/home/pi/scripts/auto_poll_server.sh &

	#Check all is still good in 30 seconds.
	sleep 30
done
