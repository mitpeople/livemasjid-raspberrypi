#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

#Set configuration variables
set -a; . ~/scripts/config.sh ; set +a

#Define config variables if not set
if [[ -z $tunnel_monitor ]]; then
        tunnel_monitor=2000
fi
if [[ -z $tunnel_port ]]; then
        tunnel_port=3000
fi
if [[ -z $tunnel_host ]]; then
        tunnel_host="livemasjid.com"
fi

if [[ "$1" == "stop" ]]; then
    log "STOP signal recieved, killing any tunnel parts using 'localhost:22.*${tunnel_host}'"
    pkill -9 -f "localhost:22.*${tunnel_host}";
    exit $?;
fi

log "Poll Server for availible tunnel ports"
server_response=$(curl -s -w "\\n%{http_code}" "https://www.${tunnel_host}/api/get_ssh_tunnel_ports.php?mount=${mount}")
#server_response=$(curl -s -w "\\n%{http_code}" "https://www.${tunnel_host}/api/get_ssh_tunnel_ports.php?mount=northcliff")
status_code=`echo "$server_response" | tail -1`
log "Server returned status code: ${status_code} and content: ${server_response}";
if [[ $status_code -eq 200 ]]; then
    ports=`echo "${server_response}" | head -n -1`
    if [ -z "$ports" ]
        then
            log "No port values returned from server... exiting"
	    exit 1;
        else
            log "Some data returned, maybe has Port values?"
	    aport=$(echo $ports | sed -n -e 's/\(.*\)|||\(.*\)/\1/p')
	    mport=$(echo $ports | sed -n -e 's/\(.*\)|||\(.*\)/\2/p')
	    log "found aport='$aport' and mport='$mport'"
	    if [[ $aport =~ ^[0-9]+$ ]] && [[ $mport =~ ^[0-9]+$ ]]; then
		log "Port values are numeric and will be used for furture connections"
		tunnel_port=$aport;
		tunnel_monitor=$mport;
		sed -i -e "s/^tunnel_port.\+$/tunnel_port\=\"${tunnel_port}\"/" /home/pi/scripts/config.sh
		sed -i -e "s/^tunnel_monitor.\+$/tunnel_monitor\=\"${tunnel_monitor}\"/" /home/pi/scripts/config.sh
	    else
		log "Port values are not numeric? using local config ports"
	    fi
    fi
elif [[ $status_code -eq 403 ]]; then
    log "Mount not configured on the server. status code: ${status_code}";
    exit 2;
else
    log "Server returned status code: ${status_code} and content: ${server_response}";
    exit 1;
fi

log "Tunnel not up, check for connectivity and try establish connection on ports '$tunnel_port' and '$tunnel_monitor'"
ping -q -c3 $tunnel_host > /dev/null 2>&1
if [ $? -eq 0 ]; then
    pkill -9 -f "localhost:22.*${tunnel_host}"
    autossh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ExitOnForwardFailure=yes -o TCPKeepAlive=yes -o ServerAliveInterval=30 -o ServerAliveCountMax=5 -N -M $tunnel_monitor -R $tunnel_port:localhost:22 pi@$tunnel_host 2>&1 &
    log "Connected and able to ping ${tunnel_host}"
    setStatus "Internet" "0" "Connected and able to ping ${tunnel_host}" ;
else
    log "No Internet Connectivity - Cannot ping ${tunnel_host}"
    setStatus "Internet" "3" "No Internet Connectivity - Cannot ping ${tunnel_host}" ;
fi
