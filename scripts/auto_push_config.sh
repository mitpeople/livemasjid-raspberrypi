#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

#Set configuration variables
set -a; . ~/scripts/config.sh ;set +a

#Define config variables if not set
if [[ -z $mqtthost ]]; then
      mqtthost="livemasjid.com"
fi
if [[ -z $mqttport ]]; then
      mqttport=1883
fi
if [[ -z $mqttuser ]]; then
      mqttuser="securepi"
fi
if [[ -z $mqttpass ]]; then
      mqttpass="ALuwa16my3ysc990"
fi
if [[ -z $mount ]]; then
       mount=$(hostname)
fi

while true;
do
###############################################
#Ensure We have Internet Connection
nc -z 8.8.8.8 53 >/dev/null 2>&1
online=$?
if [ $online -ne 0 ]; then
    log "No Internet Connection"
    sleep 60;
    #exit 64
else
    log "Internet Connection Found"
    break;
fi
##############################################
done

pi_serial=$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2);

#push mic_level
cur_cap_volume=$(amixer -c1 get Mic | grep "Capture.*\[[[:digit:]]\+%\]" | sed -n -e 's/.\+Capture.\+\[\(.\+\)\%.\+]/\1/p');
log "Publishing message informing current mic level at ${cur_cap_volume}%";
mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mic_level" -m "${cur_cap_volume}" -q 2 -u $mqttuser -P $mqttpass;

#push mic_threshold
mic_threshold=$(cat /home/pi/scripts/masjid_opus.liq | grep "^[^#].*threshold=" | sed -n -e 's/.\+threshold=\(.\+\)\..\+/\1/p');
log "Publishing message informing current mic threshold at ${mic_threshold}";
mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mic_threshold" -m "${mic_threshold}" -q 2 -u $mqttuser -P $mqttpass;
