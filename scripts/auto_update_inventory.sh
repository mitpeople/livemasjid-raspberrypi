#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
	function log () {
    		if [[ $_V -eq 1 ]]; then
        		echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
    		fi
	}
fi

while true;
do
###############################################
#Ensure We have Internet Connection
nc -z 8.8.8.8 53 >/dev/null 2>&1
online=$?
if [ $online -ne 0 ]; then
    log "No Internet Connection"
    sleep 60;
    #exit 64
else
    log "Internet Connection Found"
    break;
fi
##############################################
done

hour=$(date "+%-H")
minute=$(date "+%-M")
second=$(date "+%-S")
#Origionally run every hour
#if [ "$hour" == "1" ] && [ "$minute" == "0" ] && [ "$second" -lt "30" ]; then

    log "Running Inventory Check";
    function checkandreport () {
        item=$1
        value=$2
	value=`echo $value | sed -e 's/[()\/&]//g'`

	if [ -z "${!item+x}" ]
	then
	    log "Inventory does not have record for ${item} will add it";
	    echo "${item}=\"\"" >> ~/scripts/inventory
	fi

        if [ "${!item}" != "${value}" ]
        then
            log "Inventory change for ${item} from '${!item}' to current value of '${value}'"
            #Send the update to backend server
            server_response=`curl -s -w "\\n%{http_code}\\n" -X POST --data "mount=(${mount})&item=(${item})&value=(${value})" https://www.livemasjid.com/api/inventory_update.php`
            status_code=`echo -e "$server_response" | tail -1`
            if [[ $status_code -eq 200 ]]; then
                log "Successfully Updated ${item} to '${value}' on the server"
                #Update local record if update submitted successfully
                sed -i -e "s/^${item}\=.\+$/${item}\=\"${value}\"/" ~/scripts/inventory
            fi
        fi
    }

    mount=$(hostname)

    #Load current values into environment variables
    set -a; . ~/scripts/inventory; set +a

    #Hardware version
    value=$(tr -d '\0' < /proc/device-tree/model)
    item="hardware_version"
    checkandreport "$item" "$value"
    #Hardware type
    if [[ $value == *"Raspberry Pi"* ]]; then
	value="Pi"
	item="hardware_type"
	checkandreport "$item" "$value"
    fi

    #OS Version
    value=$(cat /etc/os-release | grep "PRETTY_NAME=" | sed -n -e 's/^PRETTY_NAME=\"\(.*\)\"/\1/p')
    item="os_version"
    checkandreport "$item" "$value"

    #Software Version
    value=$(cat ~/scripts/version | grep Version | head -1 | sed -n -e 's/^Version \(.*\)/\1/p')
    item="software_version"
    checkandreport "$item" "$value"

    #storgae size
    value=$(lsblk | grep disk | awk '{print $4}')
    item="storgage_size"
    checkandreport "$item" "$value"

    #Audio Card
    value=$(lsusb -v 2>/dev/null | egrep '^Bus |bInterfaceClass.+Audio' | grep -B 1 Audio | grep Bus | sed -n -e 's/^Bus.*ID \(.*\)/\1/p')
    item="audio_card"
    checkandreport "$item" "$value"

    #Connection Method
    value=$(cat ~/scripts/config.sh | grep "internet_access=" | sed -n -e 's/^internet_access\=\"\(.*\)\"/\1/p')
    item="connection_type"
    checkandreport "$item" "$value"

    if [ "${value}" == "USBModem" ]
    then
	#modem ID
	modemdev=$(dmesg | egrep 'usb [[:digit:]].*modem.* tty' | tail -1 | sed -n -e 's/^\[.*usb \(.*\):.*/\1/p')
	vendorid=$(cat /sys/bus/usb/devices/${modemdev}/idVendor)
	productid=$(cat /sys/bus/usb/devices/${modemdev}/idProduct)
	value="${vendorid}:${productid}"
	item="modem_id"
	checkandreport "$item" "$value"


        mmcliinfo=$(mmcli -m 0)

	#Modem
	#value=`echo -e "$sakisinfo" | grep "Modem:" | sed -n -e 's/^Modem: \(.*\)/\1/p'`
	#item="modem"
	#checkandreport "$item" "$value"
	#sim_operator
	value=$(echo -e "$mmcliinfo" | grep "operator name:" | sed -n -e 's/^.*operator name: \(.*\)/\1/p')
	item="sim_operator"
	checkandreport "$item" "$value"

	value=$(echo -e "$mmcliinfo" | grep "signal quality:" | sed -n -e 's/^.*signal quality: \(.*%\).*/\1/p')
	item="signal_strength"
	checkandreport "$item" "$value"

	value=$(echo -e "$mmcliinfo" | grep "access tech:" | sed -n -e 's/^.*access tech: \(.*\)/\1/p')
	item="modem_access"
	checkandreport "$item" "$value"

    else
	value=""
        item="modem_id"
        checkandreport "$item" "$value"

	value=""
	item="modem"
        checkandreport "$item" "$value"

	value=""
	item="sim_operator"
        checkandreport "$item" "$value"
    fi

#fi
