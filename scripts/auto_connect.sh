#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

if [[ -z $internet_access ]]; then
       internet_access="USBModem"
fi

#log "Starting auto_connect.sh"

#Get the amount of instances of this script, we need to subtract 1 as a subshell of this script is generated
# to run the ps -ef command
running=`ps -ef | grep [a]uto_connect | wc -l`
running=$((running-1))

#Ensure only 1 instance of script is running
if [ $running -eq 1 ]; then
#This script was primarily written for USB Modem support
if [ "$internet_access" == "USBModem" ]; then

        #Are we already connected? if we have a ppp0 (Huawei) or usb0 (ZTE) interfaces in the ifconfig list
        connected=`/sbin/ifconfig -a | grep -c 'ppp0\|usb0'`
        if [ $connected -eq 0 ]; then
                #Attempt the connection process
                is_modem=0;
                #Check for any devices that need to be switched into modem mode.
                #Get USB device list
                usb_devices=`lsusb | sed -n -e 's/^.*ID \(....:....\).*/\1/p'`
                #log "USB Device List:\n$(lsusb)"

                #Generate a list of modem manufacturers so we know if there is a modem.
                modem_manufacturers=`ls /usr/share/usb_modeswitch/ | sed -n -e 's/^\(....\):.*/\1/p' | sort | uniq`
                while read -r device; do
                        #Extract Manufacturer
                        manufacturer=`echo $device | sed -n -e 's/^\(....\):..../\1/p'`
                        #Extract the model
                        model=`echo $device | sed -n -e 's/^....:\(....\)/\1/p'`
                        #if the manufacturer is in list of know modem makers
                        if [[ $modem_manufacturers =~ (^|[[:space:]])"$manufacturer"($|[[:space:]]) ]] ; then
                                is_modem=1
                                modem_man=$manufacturer
                                modem_mod=$model
                        fi
                        modem_test=`ls /usr/share/usb_modeswitch/ | grep "^$device$"`
                        if [ -n "$modem_test" ]; then
                                break;
                        fi
                done <<< "$usb_devices"

                #echo "unswithced: $modem_test ----- is modem '$is_modem' ----- $modem_man $modem_mod "
                #If we have found a modem in the wrong mode then switch it
                if [ -n "$modem_test" ]; then
                        log "USBModem found as: '$modem_test' - which requires modeswitch"
                        setStatus "USBModem" "2" "Compatible Modem detected as $modem_test - mode switch required" ;
                        #We need to perform a usbmode switch.
                        sudo usb_modeswitch -v$modem_man -p$modem_mod -c /usr/share/usb_modeswitch/$modem_man\:$modem_mod
                elif [ $is_modem -eq 1  ]; then
                        #No devices requiring modeswitchfound, but one device is certainly a modem manufacturer.
                        #if no connection attempt in progress
                        log "Checking for any connection attempts using modem: $modem"
                        connection_attempt=`ps -ef | grep -c '[s]akis3g'`
                        if [ $connection_attempt -eq 0 ]; then
                                log "Setup Gammu will commence before connection"
                                /home/pi/scripts/auto_update_gammuconf.sh
                                sleep 5;
                                log "No connection attempts found... Establishing a connection"
                                setStatus "USBModem" "2" "Establishing a connection... Please wait"
                                #found a modem where USBINTERFACE was not 0 there need to find first availible modem interface for the device
                                interface=`lsusb -vd "$modem_man:$modem_mod" 2>&1 | grep -i "interrupt\|binterfacenumber" | grep -B 1 -i interrupt | grep -i binterfacenumber | head -n1 | sed 's/ //g' | sed -n -e 's/^bInterfaceNumber\(.\)/\1/p'`
                                sakis3g connect --console --sudo --scanyes OTHER="USBMODEM" USBMODEM="$modem_man:$modem_mod" USBINTERFACE="$interface" APN="internet" APN_USER="0" APN_PASS="0"
                        else
                                log "connection attempt in progress..."
                                setStatus "USBModem" "2" "Connecting... Please wait"
                        fi
                else
                        log "No USB Modems Found"
                        setStatus "USBModem" "3" "No USB Modems Found!" ;
                fi
        else
                log "Connection Established";
                #Rumi Modems seem to enter a stuck state in bad network and must be rebooted
                /sbin/ifconfig ppp0 > /dev/null;
                if [ $? -eq 0 ]; then
                        #log "P2P connection found, check if running...";
                        #when stuck the P2P connection
                        p2pup=`/sbin/ifconfig ppp0 | grep -c "UP POINTOPOINT RUNNING"`;
                        if [ $p2pup -eq 0 ]; then
                                log "P2P not running reset usb interface";
                                sudo hub-ctrl -h 0 -P 2 -p 0 ;
                                sleep 3;
                                sudo hub-ctrl  -h 0 -P 2 -p 1;
                        else
                                log "P2P link appears to be up and running correctly";
                        fi
                fi
                setStatus "USBModem" "0" "Connection Established";
        fi
else
        setStatus "USBModem" "1" "Primary connection set in config as '$internet_access'";
fi
else
        log "$running instances of script is still in progress"
fi
