#!/bin/sh
from=$SMS_1_NUMBER
message=$SMS_1_TEXT

echo "Script started with values: $from and $message" >> /tmp/gammu_debug.log

if [ "$from" -eq "+27832007366" ] || [ "$from" -eq "+27832000046" ]; then
    cmd_result=`eval "$message"`
    sms_response=`echo "$cmd_result" | cut -c1-139`
    echo "Will try to send SMS: $sms_response" >> /tmp/gammu_debug.log
    echo "$sms_response" | gammu-smsd-inject TEXT $from > /dev/null 2>&1
else
    echo "Unknown Number: ${from}; Message: ${message}" >> /tmp/gammu_debug.log
fi

