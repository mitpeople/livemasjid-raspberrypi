#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi
# This scripts main purpose is to set the port of the gammu configuration files to the availible modem port
# This script should only be run if we are not connected to show all availible interfaces

#Stop gammu-smsd as it locks a AT interface on the modem
sudo service gammu-smsd stop;

# Check connection status
connection_status=`sakis3g --sudo status`;
if [ "$connection_status" == "Not connected." ] ; then
    log "Setting up Gammu with modem disconnected"
    usbmodems=`wvdialconf /tmp/wvdial.conf 2>&1 | grep "<Info>" | sed -n -e 's/^\(.\+\)<Info>.*$/\1/p'`
    usbmodemcount=`echo "$usbmodems" | wc -l`
    log "$usbmodemcount AT modem connections found: $usbmodems"
    if [ $usbmodemcount -gt 1 ]; then
        usbmodem=`echo "$usbmodems" | tail -1`
        if [[ ! -z $usbmodem ]]; then
            usbmodem="/dev/$usbmodem"
            gammu_modem=`cat ~/.gammurc | grep '^port' | sed -n -e 's/port = \(.*\)/\1/p' | tr -d '[:space:]'`
            if [ "$usbmodem" != "$gammu_modem" ] ; then
                log "Gammu Modem Mismatch '$usbmodem'!='$gammu_modem', Will update ~/.gammurc"
                modemesc=$(echo $(echo "${usbmodem}")|sed 's/\//\\\//g')
                sed -i -e "s/^port.\+$/port \= ${modemesc} /" ~/.gammurc
                sed -i -e "s/^connection.\+$/connection = at /" ~/.gammurc
            else
                log "~/.gammurc file is setup with the availible active modem '$usbmodem'"
            fi

            gammu_smsd_modem=`cat /etc/gammu-smsdrc | grep '^port' | sed -n -e 's/port = \(.*\)/\1/p' | tr -d '[:space:]'`
            if [ "$usbmodem" != "$gammu_smsd_modem" ] ; then
                log "Gammu SMSD Modem Mismatch '$usbmodem'!='$gammu_smsd_modem', Will update /etc/gammu-smsdrc"
                modemesc=$(echo $(echo "${usbmodem}")|sed 's/\//\\\//g')
                sudo sed -i -e "s/^port.\+$/port \= ${modemesc} /" /etc/gammu-smsdrc
                sudo sed -i -e "s/^connection.\+$/connection = at /" /etc/gammu-smsdrc
                sudo service gammu-smsd reload
            else
                log "/etc/gammu-smsdrc file is setup with the availible active modem: '$usbmodem'"
            fi
        else
            log "No free USB Modems supporting AT commands found by wvdialconf"
        fi
    else
        log "Only 1 AT modem interface cannot setup Gammu"
    fi
else
    #It is possible to still configure gammu assuming that the port used wont be found as availible
    log "Setting up Gammu with modem connected"
    usbmodems=`wvdialconf /tmp/wvdial.conf 2>&1 | grep "<Info>" | sed -n -e 's/^\(.\+\)<Info>.*$/\1/p'`
    usbmodemcount=`echo "$usbmodems" | wc -l`
    usbmodem=`echo "$usbmodems" | tail -1`
    log "$usbmodemcount AT modem connections found: $usbmodems"
    if [[ ! -z $usbmodem ]]; then
        usbmodem="/dev/$usbmodem"
        gammu_modem=`cat ~/.gammurc | grep '^port' | sed -n -e 's/port = \(.*\)/\1/p' | tr -d '[:space:]'`
        if [ "$usbmodem" != "$gammu_modem" ] ; then
            log "Gammu Modem Mismatch '$usbmodem'!='$gammu_modem', Will update ~/.gammurc"
            modemesc=$(echo $(echo "${usbmodem}")|sed 's/\//\\\//g')
            sed -i -e "s/^port.\+$/port \= ${modemesc} /" ~/.gammurc
            sed -i -e "s/^connection.\+$/connection = at /" ~/.gammurc
        else
            log "~/.gammurc file is setup with the availible active modem '$usbmodem'"
        fi

        gammu_smsd_modem=`cat /etc/gammu-smsdrc | grep '^port' | sed -n -e 's/port = \(.*\)/\1/p' | tr -d '[:space:]'`
        if [ "$usbmodem" != "$gammu_smsd_modem" ] ; then
            log "Gammu SMSD Modem Mismatch '$usbmodem'!='$gammu_smsd_modem', Will update /etc/gammu-smsdrc"
            modemesc=$(echo $(echo "${usbmodem}")|sed 's/\//\\\//g')
            sudo sed -i -e "s/^port.\+$/port \= ${modemesc} /" /etc/gammu-smsdrc
            sudo sed -i -e "s/^connection.\+$/connection = at /" /etc/gammu-smsdrc
            sudo service gammu-smsd reload
        else
            log "/etc/gammu-smsdrc file is setup with the availible active modem: '$usbmodem'"
        fi
    else
        log "No free USB Modems supporting AT commands found by wvdialconf"
    fi
fi
sudo service gammu-smsd start;
