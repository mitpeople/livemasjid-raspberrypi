#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

#Set configuration variables
set -a;. ~/scripts/config.sh;set +a;

#Define config variables if not set
if [[ -z $maxDiskLimit ]]; then
       maxDiskLimit=90
fi

#Check Disk Space on recordings mount
disk_used=`df -h | grep "arecordings" | awk '{print $5}' | sed 's/%//'`

#Check if disk space is over the limit
while [ $disk_used -gt $maxDiskLimit ]
do
    log "Disk Usage ($disk_used) found to be over the limit ($maxDiskLimit). Will clean up oldest file"

    file2rm=$(find /home/pi/arecordings/ \( -iname '*.mp3' -o -iname '*.ogg' \) -type f -printf "%T@ %p\n" | sort -r | tail -1 | sed 's/[^ ]* //' );
    log "Removing: $file2rm"
    rm "$file2rm"

    disk_used=`df -h | grep "arecordings" | awk '{print $5}' | sed 's/%//'`
    sleep 1;
done
