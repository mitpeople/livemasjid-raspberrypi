#This file will contain all the variable parameters required for the stream.
#Please set all the parameters accordingly to prevent errors.

#Streaming Details:
#The icecast mount that your stream is called
mount="version10pi"
streamdesc="Audio stream ..."
streamurl="http://livemasjid.com/v10"

#Mode of internet connection
#Valid values are: USBModem, Ethernet or WiFi
internet_access="USBModem"

#USBModem Settings:
apn="internet"

#If WiFi is set for internet_access then an Access Point Name and Password is required
#This is only applicable if wifi is not used for the actual internet connection
#This will allow the Raspberry to broadcast itself over wifi for admin to connect to
#The WiFi interface allows for esy access to debug monitoring and configuration adjustment.

#1=Enable (default), 0=Disable
enableWiFiAP="1"
#The name of the Wifi Access Point Broadcasted
apName="Pi3-Streamer"
#The password to connect to this access point
apPassword="QWERTYUI"

#Webserver Interface
#This enables the apache config which allows for webbased configuration management
#1=Enable (default), 0=Disable
enableWebInterface="1"

#Reverse tunnel configuration, for remote access and administration,
#Please choose ports availible by consulting Wiki
tunnel_port="2989"
tunnel_monitor="2999"

#Streaming Settings
sbitrate="32"
#Samplerate is limited by the USB Audio card capability, 44100 is the default and should not be changed.
ssample_rate="44100"

#Local file recording settings, this is files stored on the Pi.
lbitrate="32"
#Samplerate is limited by the USB Audio card capability, 44100 is the default and should not be changed.
lsample_rate="44100"
#This sets the disk full percenatge limit, only when this limit is reached will the recordings be deleted from oldest.
maxDiskLimit="90"

#Ram Space Allocation (48,64,96,128)
#Pi 1 recommended is 48,
#Pi 2 recommended is 64 or 96,
#Pi 3 recommended is 96, 128 or more
#This space is be used to store recordings before they are dumped to disk.
ram="192"

#Enable the scheduling
enable_scheduler="1"

#Vebose flag, (1) enabling logging, (0) disable logging
_V="1"
