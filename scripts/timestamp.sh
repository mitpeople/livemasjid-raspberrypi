#!/bin/bash
if [ -f "/home/pi/ram/dump.mp3" ]
then
        mv /home/pi/ram/dump.mp3 /home/pi/arecordings/$(date +%Y%m%d%H%M%S)_livedump.mp3
fi
if [ -f "/home/pi/ram/dump.ogg" ]
then
        mv /home/pi/ram/dump.ogg /home/pi/arecordings/$(date +%Y%m%d%H%M%S)_livedump.ogg
fi

