#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

set -a;. ~/scripts/config.sh;set +a;

#Define config variables if not set
if [[ -z $enableWiFiAP ]]; then
       enableWiFiAP=1
fi
if [[ -z $apName ]]; then
       apName="Pi3-Streamer"
fi
if [[ -z $apPassword ]]; then
       apPassword="QWERTYUI"
fi
if [[ -z $enableWebInterface ]]; then
       enableWebInterface=1
fi
if [[ -z $ram ]]; then
       ram=48
fi

log "Bootup Setup running..."

#Expand the filesystem to SD disk size
#/home/pi/scripts/auto_expand_sd.sh

#Create/Set the RAM Mount
#Will be good to first check what size is alocated before we remount... TBD
current_RAM=`df -ah | grep ram | tr -s ' ' | cut -d ' ' -f2 | sed 's/M//'`
if [ "$current_RAM" != "$ram" ] ; then
        log "RAM Mismatch... configuration set as ${ram}M and current RAM equals ${current_RAM}M"
        log "Altering RAM space to ${ram}M"
	sudo mount -o remount,size=${ram}m,defaults,noatime,nosuid /home/pi/ram
fi

#This file will be used to exchange status infomation of processes
touch /home/pi/ram/sysmonitor.data
chmod 666 /home/pi/ram/sysmonitor.data

#Ensure we are not muted...
#The web interface allows for mic muting, if an unexpected reboot occurs this could force
#the mic to remain in the muted state
audiocapture=`arecord -l | grep -c card`;
if [[ $audiocapture -ne 0 ]]; then
    mic_mute=`amixer -c1 | grep -e 'Capture.\+\[' | sed -n -e 's/.\+\[\(\(on\|off\)\)]/\1/p'`
    if [ "$mic_mute" == "off" ] ; then
        log "Mic found in muted state... turn on"
	amixer -c1 set Mic cap
    fi
fi

#WiFi AP Management
log "Should WiFi AP be Enabled?..."
if [[ $enableWiFiAP -eq 1 ]];
then
	#Cause hostapd to start at bootup automatically
	sudo update-rc.d hostapd enable
	log "Enabling WiFi AP with the following details:"
	log "APName '$apName', AP Password '$apPassword' "

	curr_APName=`cat /etc/hostapd/hostapd.conf | grep '^ssid=' | sed -n -e 's/ssid=\(.\+\)$/\1/p'`
	log "Current AP Name : $curr_APName "
	#Check if the configured AP name is equal to current AP name
	if [ "$curr_APName" != "$apName" ] ; then
		log "AP Name Mismatch, Update AP and restart"
		sudo sed -i -e "s/ssid.\+$/ssid=${apName}/" /etc/hostapd/hostapd.conf
		sudo service hostapd restart
	else
		log "No AP Name mismatch found, no need to change conf file"
	fi

	curr_APPass=`cat /etc/hostapd/hostapd.conf | grep '^wpa_passphrase=' | sed -n -e 's/wpa_passphrase=\(.\+\)$/\1/p'`
        log "Current AP Password : $curr_APPass "
	#Check if the configured AP Passphrase is equal to current AP Passphrase
        if [ "$curr_APPass" != "$apPassword" ] ; then
                log "AP Passphrase Mismatch, Update AP Passphrase and restart"
		sudo sed -i -e "s/wpa_passphrase.\+$/wpa_passphrase=${apPassword}/" /etc/hostapd/hostapd.conf
		sudo service hostapd restart
        else
                log "No AP Passphrase mismatch found, no need to change conf file"
        fi

	#Check if hostapd is running, if not bring it up.
	hostap_up=`/usr/sbin/service hostapd status | grep Active: | sed -n -e 's/.\+Active: \(.\+\) (.\+/\1/p'`
	if [ "$hostap_up" == "inactive" ] ; then
                log "hostapd service is inactive and must be started"
                sudo service hostapd start
        else
                log "Host AP is up and : $hostap_up"
        fi

else
	log "Disable WiFi Access Point Service"
	sudo update-rc.d hostapd disable
	sudo service hostapd stop
fi

#Apache Webinterface management
log "Should Apache be Enabled?..."
if [[ $enableWebInterface -eq 1 ]];
then
        log "Enabling Apache Service..."
        sudo update-rc.d apache2 enable
	sudo service apache2 start
else
        log "Disable Access Point Service..."
        sudo update-rc.d apache2 disable
	sudo service apache2 stop
fi

#Check hostname and ensure it is same as the mountname
curr_hostname=`hostname | sed 's/ //g'`
if [[ -z $mount ]]; then
       mount="raspberrypi"
fi

if [ "$curr_hostname" != "$mount" ] ; then
      	log "Hostname Mismatch '$curr_hostname'!='$mount', Will update hostname"
	sudo sed -i -e "s/127\.0\.1\.1.\+$/127\.0\.1\.1       ${mount}/" /etc/hosts
	sudo sh -c "echo '$mount' > /etc/hostname"
	#depreciated
	#sudo /etc/init.d/hostname.sh
        sudo hostnamectl set-hostname "${mount}"
else
	log "No hostname mismatch found."
fi

#Check APN is correct
if [[ "${internet_access}" == "USBModem" ]] ; then
    curr_apn=$(nmcli -f gsm.apn connection show 3GModem | tr -s [:blank:] | cut -d " " -f 2)
    if [[ "${curr_apn}" != "${apn}" ]] ; then
        log "Current APN: ${curr_apn} does not match configured APN: ${apn}. Updating..."
        sudo nmcli con modify 3GModem apn "#{apn}"
    else
        log "Current APN: ${curr_apn} matches configured APN: ${apn}."
    fi
fi

#Push inventory to server
/home/pi/scripts/auto_update_inventory.sh &
