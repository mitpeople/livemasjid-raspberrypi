#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

#Set configuration variables
set -a ; . ~/scripts/config.sh ; set +a

#Define config variables if not set
if [[ -z $mqtthost ]]; then
      mqtthost="livemasjid.com"
fi
if [[ -z $mqttport ]]; then
      mqttport=1883
fi
if [[ -z $mqttuser ]]; then
      mqttuser="securepi"
fi
if [[ -z $mqttpass ]]; then
      mqttpass="ALuwa16my3ysc990"
fi
if [[ -z $mount ]]; then
       mount=$(hostname)
fi

pi_serial=$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2);
p="/home/pi/scripts/mqttpipe"

#Setup FIFO pipe to MQTT Server
log "Connecting to server ${mqtthost} and listening on topic pi/${pi_serial}/#"
([ ! -p "$p" ]) && mkfifo $p
(mosquitto_sub -v -h $mqtthost -p $mqttport -t "pi/${pi_serial}/#" -u $mqttuser -P $mqttpass >$p 2>/dev/null) &

while read mqtt_msg <$p
do
  log "MQTT Message arrived: ${mqtt_msg}"
  topic=${mqtt_msg%% *};
  payload=${mqtt_msg#* };
  log "Processing MQTT message with topic: ${topic} and Payload ${payload}"
  #mqtt_mount=$(echo ${topic} | cut -d"/" -f3);
  property=$(echo ${topic} | cut -d"/" -f3);
  action=$(echo ${topic} | cut -d"/" -f4);

  log "Checking for property ${property} and action ${action}"
  if [[ "${property}" == "ping" ]]; then
    log "Property Ping found, reply with a pong."
    mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/pong" -m "$(date)" -q 2 -u $mqttuser -P $mqttpass;

  elif [[ "${property}" == "mount" ]]; then
    log "Property mount found"
    if [[ "${action}" == "set" ]]; then
      #First do data validation to ensure it is in the RADXXX format TBD
      #if [[ "$payload" -ge "0" && "$payload" -le "100" ]]; then
        log "Setting mount name to ${payload}";
	sed -i -e "s/^mount\=.\+$/mount\=\"${payload}\"/" /home/pi/scripts/config.sh
        #publish new mountname as confimation
        mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mount" -m "${payload}" -q 2 -u $mqttuser -P $mqttpass;
      #else
      #  log "ERROR: invalid value ${payload} for volume property";
      #fi

    elif [[ "${action}" == "get" ]]; then
      log "Publishing message informing current mount name set as ${mount}";
      mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mount" -m "${mount}" -q 2 -u $mqttuser -P $mqttpass;
    fi

  elif [[ "${property}" == "mic_level" ]]; then
    log "Property mic_level found"
    if [[ "${action}" == "set" ]]; then
      log "action set found for mic_level with payload ${payload}"
      payload=${payload%.*}
      #First do data validation to ensure that mic level is between 0 and 100%
      if [[ "${payload}" -ge "0" && "${payload}" -le "100" ]]; then
        log "Setting mic_level to ${payload}";
        amixer -q -c1 set Mic Capture "${payload}%"
        sudo alsactl store
        #publish new mic_level as confimation
        cur_mic_level=$(amixer -c1 get Mic | grep "Capture.*\[[[:digit:]]\+%\]" | sed -n -e 's/.\+Capture.\+\[\(.\+\)\%.\+]/\1/p');
        mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mic_level" -m "${cur_mic_level}" -q 2 -u $mqttuser -P $mqttpass;
      else
        log "ERROR: invalid value ${payload} for mic_level property";
      fi

    elif [[ "${action}" == "get" ]]; then
      cur_mic_level=$(amixer -c1 get Mic | grep "Capture.*\[[[:digit:]]\+%\]" | sed -n -e 's/.\+Capture.\+\[\(.\+\)\%.\+]/\1/p');
      log "Publishing message informing current mic_level set as ${cur_mic_level}";
      mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mic_level" -m "${cur_mic_level}" -q 2 -u $mqttuser -P $mqttpass;
    fi

  elif [[ "${property}" == "mic_threshold" ]]; then
    log "Property mic_threshold found"
    if [[ "${action}" == "set" ]]; then
      log "action set found for mic_threshold with payload ${payload}"
      payload=${payload%.*}
      #First do data validation to ensure that mic level is between 0 and 100%
      if [[ "${payload}" -ge "-50" && "${payload}" -le "0" ]]; then
        log "Setting mic_threshold to ${payload}";
        sed -i -E "s/(threshold=-?[0-9]*)/threshold=${payload}/" /home/pi/scripts/masjid_opus.liq
        #kill liquidsoap so that it restarts with new config
        echo "0" > /home/pi/ram/liquidsoap_runcount.txt
        pkill -f liquidsoap
        #publish new mic_threshold as confimation
        cur_mic_threshold=$(cat /home/pi/scripts/masjid_opus.liq | grep "threshold=" | sed -n -E 's/.+threshold=(-?[0-9]+).+/\1/p');
        mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mic_threshold" -m "${cur_mic_threshold}" -q 2 -u $mqttuser -P $mqttpass;
      else
        log "ERROR: invalid value ${payload} for mic_threshold property";
      fi

    elif [[ "${action}" == "get" ]]; then
      mic_threshold=$(cat /home/pi/scripts/masjid_opus.liq | grep "^[^#].*threshold=" | sed -n -e 's/.\+threshold=\(.\+\)\..\+/\1/p');
      log "Publishing message informing current mic_threshold set as ${mic_threshold}";
      mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/mic_threshold" -m "${mic_threshold}" -q 2 -u $mqttuser -P $mqttpass;
    fi

  elif [[ "${property}" == "recordings" ]]; then
    log "Property recordings found"
    if [[ "${action}" == "list" ]]; then
      log "action list found for recordings with payload ${payload}"
      if [[ "${payload}" == "(null)" ]]; then
        #list all recordings
        log "Listing ALL recordings";
        for filename in /home/pi/arecordings/*.mp3 ; do
           JSON_FMT='{"recname":"%s","recduration":"%s"}'
	   recording_file=$(basename "$filename")
           rec_duration=$(mp3info -p "%S" "$filename")
           rec_json=$(printf "$JSON_FMT" "$recording_file" "$rec_duration" )
           log "publishing recording: ${rec_json}"
           mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/recording/" -m "${rec_json}" -q 2 -u $mqttuser -P $mqttpass;
        done
      else
        log "ERROR: still need to implement pre wildcard";
      fi

    elif [[ "${action}" == "get" ]]; then
      #Need to upload a recording to the pistaging bucket.
      #check if file exists ${payload}
      log "Publishing message informing status of request";
      mosquitto_pub -h $mqtthost -p $mqttport -t "pi/${pi_serial}/recordings/upload" -m "${upload_status}" -q 2 -u $mqttuser -P $mqttpass;
    fi

  else
    log "Unknown/Unsupported mqtt command.";
  fi
done
