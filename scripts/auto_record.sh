#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
	function log () {
    		if [[ $_V -eq 1 ]]; then
        		echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
    		fi
	}
fi

#Set configuration variables
set -a; . ~/scripts/config.sh; set +a;

#Define config variables if not set
if [[ -z $lbitrate ]]; then
       lbitrate=32
fi
if [[ -z $lsample_rate ]]; then
       lsample_rate=44100
fi

log "Checking for running arecord..."
#is arecord running?
record=`pgrep -c 'arecord'`
if [ $record -le "0" ]; then
    log "arecord not running... will attempt to start arecord till the top of the hour"
    setStatus "LocalRecording" "2" "Local Recording Not running" ;
    #The idea is to record in hourly chunks i.e. have a file per hour.
    #To accomplish this we can set the duration of each run

    #Might be a good idea to check if we have a sound card to prevent errors in the logs
    audiocapture=`arecord -l | grep -c card`;
    if [[ $audiocapture -ne 0 ]]; then
        read min sec <<<$(date +'%M %S')
        diff=$(( 3600 - 10#$min*60 - 10#$sec ))
	arecord -q -f S16_LE -c1 -r$lsample_rate -t wav -D stream -d $diff - | lame -mm -f -b$lbitrate --resample 44.1 - "/home/pi/ram/$(date +%Y%m%d%H%M%S)_backup.mp3" &
    else
	log "No audio capture device found."
        setStatus "LocalRecording" "3" "No audio capture devices found" ;
    fi
else
    log "arecord found and running"
    setStatus "LocalRecording" "0" "Local Recording active" ;
    #move any old files
    find /home/pi/ram/  -iname '*_backup.mp3' -type f -mmin +61 -exec mv -t /home/pi/arecordings/ {} +
fi
