#!/bin/bash
#If log was not previously defined then define it
if [[ "$(type -t log)" != "function" ]]; then
        function log () {
                if [[ $_V -eq 1 ]]; then
                        echo -e $(date +"%Y-%m-%d %H:%M:%S") ": $0: $@"
                fi
        }
fi

#Set configuration variables
set -a; . ~/scripts/config.sh; set +a;

#Define config variables if not set
if [[ -z $mount ]]; then
       export mount="undefined"
fi
if [[ -z $streamdesc ]]; then
       export streamdesc="Not set"
fi
if [[ -z $streamurl ]]; then
        export streamurl="Not set"
fi
if [[ -z $streamhost ]]; then
        export streamhost="livemasjid.com"
fi

#Each count is equivelent to 30seconds ths 120 = 1 hour.
run_count=`cat ~/ram/liquidsoap_runcount.txt`
if [[ $run_count -ge 120 ]]; then
	log "Liquidsoap runcount is now being reset after 1 hour of waiting"
	echo "0" > ~/ram/liquidsoap_runcount.txt
	exit 1;
elif [[ $run_count -ge 3 ]]; then
	log "Something is wrong liqidsoap could not start after 3 attempts will try again in an hour"
	setStatus "Streaming" "3" "Something is wrong liqidsoap could not start after 3 attempts will try again in an hour" ;
	run_count=$((run_count+1))
        echo "$run_count" > ~/ram/liquidsoap_runcount.txt
	exit 1;
else
	log "Checking for running liquidsoap..."
	#is liquidsoap running?
	liquidcheck=`ps -ef | grep -c '[l]iquidsoap'`
	#if not running restart
	if [[ $liquidcheck -le 0 ]]; then
		run_count=$((run_count+1))
		echo "$run_count" > ~/ram/liquidsoap_runcount.txt
		#Before we stream we need an audio source.
		audiocapture=`arecord -l | grep -c card`;
	        if [[ $audiocapture -ne 0 ]]; then
			log "Capture devices found: list:\n$(arecord -l)"
			setStatus "Audio" "0" "Audio capture device found" ;
			log "Starting up liquidsoap as no process found..."
			log "run count: $run_count"
			#/home/pi/scripts/masjid_vorbis.liq > /dev/null 2>&1
			/home/pi/scripts/masjid_opus.liq > /dev/null 2>&1
			#/home/pi/scripts/masjid.liq > /dev/null 2>&1
		else
			log "No capture devices found!"
			setStatus "Audio" "3" "No Audio capture device found" ;
		fi
	else
		log "Liquidsoap process found and running"
		setStatus "Streaming" "0" "Streaming service Liqidsoap is running using mount: $mount" ;
	fi
fi
