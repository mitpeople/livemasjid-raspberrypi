#!/bin/bash

capture_level=`ps -ef | grep -c -P '[a]record.+?vvv.+?'` 
if [ $capture_level -ge 1 ]; then
	echo 'we are recording'
else 
	echo 'we are not recording begin in the background now'
	touch /home/pi/ram/volume.data
	chmod 666 /home/pi/ram/volume.data
	arecord -q -f S16_LE -c1 -r44100 -D stream -d 600 -vvv /dev/null > /home/pi/ram/volume.data 2>&1 &
fi
