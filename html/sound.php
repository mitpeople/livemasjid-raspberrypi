<html> 
	<head>
	<script src="./js/jquery-3.1.0.js"></script>
    	<script src="./js/highcharts.js"></script>
    	<script src="./js/highcharts-more.js"></script>
	<script>
	function mutecall() {
	        $.ajax({url: "./mute.php", success: function(result){}});
	}
	function unmutecall() {
                $.ajax({url: "./unmute.php", success: function(result){}});
        }

	</script>
	<script>
        $(function () {

	$('#container').highcharts({
                chart: {
                    type: 'gauge',
                    plotBorderWidth: 2,
                    plotBackgroundColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                        stops: [
                            [0, '#f0e6ff'],
                            [0.3, '#FFFFFF'],
                            [1, '#f0e6ff']
                        ]
                    },
                    plotBackgroundImage: null,
                    height: 200
                },
                title: {
                    text: 'Sound meter'
                },
                pane: [{
                    startAngle: -45,
                    endAngle: 45,
                    background: null,
                    center: ['50%', '145%'],
                    size: 300
                }],
                tooltip: {
                    enabled: false
                },
                yAxis: [{
                    min: 0,
                    max: 100,
                    minorTickPosition: 'outside',
                    tickPosition: 'outside',
                    minorTickInterval: 'auto',
                    minorTickWidth: 1,
                    minorTickLength: 15,
                    minorTickColor: '#666',
                    tickPixelInterval: 30,
                    tickWidth: 2,
                    tickLength: 20,
                    tickColor: '#666',

                    labels: {
                        rotation: 'auto',
                        distance: 20
                    },
                    plotBands: [
                    {
                        from: 0,
                        to: 60,
                        color: '#33cc33',
                        innerRadius: '100%',
                        outerRadius: '110%'
                    },{
                        from: 60,
                        to: 90,
                        color: '#ff6600',
                        innerRadius: '100%',
                        outerRadius: '110%'
                    },
                    {
                        from: 90,
                        to: 100,
                        color: '#C02316',
                        innerRadius: '100%',
                        outerRadius: '110%'
                    }],
                    pane: 0,
                    title: {
                        text: 'Volume %<br/><span style="font-size:8px">Live Input</span>',
                        y: -40
                    }
                }],
                plotOptions: {
                    gauge: {
                        dataLabels: {
                            enabled: false
                        },
                        dial: {
                            radius: '100%'
                        }
                    }
                },
                series: [{
                    name: 'Volume Level',
                    data: [0],
                    yAxis: 0
                }]
            },
                // Let the music play
                function (chart) {
		   var volume;
		   var avg;
                    setInterval(function () {
			$.ajax({url: "get_audio_data.php", success: function(result){
				var values = [];
				var lines = result.split('\n');
				var regExp = /(\d+)%/;
				var sum = 0;
				for(var i = 0;i < lines.length;i++){
    					//code here using lines[i] which will give you each line
					if (lines[i].match(regExp))
					{
						matches = regExp.exec(lines[i]);
						//console.log(matches[1]);
						values.push(matches[1]);
						sum += parseInt( matches[1], 10 );
					}
				}
				volume = Math.max.apply(Math,values);
				//console.log(sum);
				avg = sum/values.length;
                                //console.log(avg);
				values = [];

                        }});
                        if (chart.series) { // the chart may be destroyed
                            var left = chart.series[0].points[0],
                                leftVal,
                                rightVal;

			    //console.log(volume);
                            leftVal = avg;
                            left.update(leftVal, false);
                            chart.redraw();
                        }
                    }, 250);
                });
        });
	</script>
</head>
<body>
<div id="container" style="width: 400px; height: 400px; margin: 0 auto"></div>
<table id="sound-controls" width="40%">
        <tr>
                <td colspan="2"> <h2> Sound Controls </h2> </td>
        </tr>
        <tr>
                <td> <input type="button" name="mute"   value="mute"  onclick="mutecall()"/>  </td>
                <td> <input type="button" name="unmute" value="unmute" onclick="unmutecall()"/> </td>
        </tr>
</table>
</body>
</html>
