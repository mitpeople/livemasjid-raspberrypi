<html>
<body>
<?php
	$parameters = array();
	$config_file = file("/home/pi/scripts/config.sh");

    	foreach($config_file as $line) {
		// do stuff with $line
		preg_match('/^([^#].+?)="?(.+?)"?$/m', $line, $matches);
		if (count($matches) > 1) {
     			//echo "<p> $matches[1] : $matches[2] </p>";
			$parameter = array($matches[1], $matches[2]);
			array_push($parameters, $parameter);
		}
	}
	//print_r($parameters);
?>

<form name='form' method='post' action="update_config.php">
<?php
	echo "<table>";
	foreach($parameters as $parameter) {
		echo "<tr>";
		echo "<td>$parameter[0]:</td>";
		echo '<td><input type="text" name="mount" value="'.$parameter[1].'" style="width: 400px;"></td>';
		echo "</tr>";
	}
	echo "</table>";
?>

<input type="submit" name="submit" value="Update Config">

</form>

</body>
</html>
