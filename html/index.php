<html>
<head>
	<link rel="stylesheet" href="./css/codemirror.css">
	<link rel="stylesheet" href="./css/statusdots.css">
	<script src="./js/jquery-3.1.0.js"></script>
	<script src="./js/codemirror.js"></script>
	<script src="./js/shell.js"></script>
	<script>
	$(function(){
		var setDotColour = function(dot,color_code) {
		var color = color_code == 0 ? "green" : color_code == 2 ? "yellow" : color_code == 3 ? "red" : "none"
                 dot.removeClass('red')
                    .removeClass('yellow')
                    .removeClass('green')
                    .removeClass('none')
                    .addClass(color);
                };

		var setDotInfo = function(dot,info) {
			dot.attr('title',info);
		};


  		var editor = CodeMirror.fromTextArea(configfile, {
    		lineNumbers: true, mode: 'shell'
  		});
		editor.setSize(800, 500);

		 setInterval(function () {
                        $.ajax({url: "get_system_stats.php",
				success: function(result){
                                	var values = [];
                                	var lines = result.split('\n');
                                	var regExp = /(\w+?),(\d),?(.+)?/;
                                	var sum = 0;
                                	for(var i = 0;i < lines.length;i++){
                                        	//code here using lines[i] which will give you each line
                                        	if (lines[i].match(regExp))
                                        	{
                                                	matches = regExp.exec(lines[i]);
                                                	//console.log(matches[3]);
							setDotColour($('#'+matches[1]),matches[2]);
							setDotInfo($('#'+matches[1]),matches[3]);
                                        	}
					}
                        	}
			})
		}, 1000);

	});
	</script>
        <script>
        function mutecall() {
                $.ajax({url: "./mute.php", success: function(result){}});
        }
        function unmutecall() {
                $.ajax({url: "./unmute.php", success: function(result){}});
        }

        </script>

</head>

<body>

<table id="dashboard" width="80%">
	<tr>
		<td colspan="7"> <h1> Status Dashboard </h1> </td>
	</tr>
	<tr>
		<td width="100"> Audio Device: </td>
		<td width="100"> Recording Locally: </td>
		<td width="100"> USB Modem: </td>
		<td width="100"> Ethernet: </td>
		<td width="100"> WiFi: </td>
		<td width="100"> Internet: </td>
		<td width="100"> Streaming Process:</td>
	</tr>
	<tr>
		<!-- dot, dot green, dot red, dot yellow -->
		<td> <span id="Audio" class="dot"><span class="dot-inner"></span></span> </td>
		<td> <span id="LocalRecording" class="dot"><span class="dot-inner"></span></span> </td>
		<td> <span id="USBModem" class="dot"><span class="dot-inner"></span></span> </td>
		<td> <span id="Ethernet" class="dot"><span class="dot-inner"></span></span> </td>
		<td> <span id="WiFi" class="dot"><span class="dot-inner"></span></span> </td>
		<td> <span id="Internet" class="dot"><span class="dot-inner"></span></span> </td>
		<td> <span id="Streaming" class="dot"><span class="dot-inner"></span></span> </td>
	</tr>
</table>
</br>

<table id="power-controls" width="40%">
	<tr>
		<td colspan="2"><h2> Power Functions </h2></td>
	</tr>
	<tr>
		<td> <a href="./reboot.php" target="_blank"><input type="button" value="Reboot" /></a> </td>
		<td> <a href="./shutdown.php" target="_blank"><input type="button" value="Shutdown" /></a> </td>
	</tr>
</table>
</br>

<table id="streaming-controls" width="50%">
	<tr>
		<td> <h2> Streaming Controls </h2> </td>
	</tr>
	<tr>
		<form action="./mute_sound.php" method="post">
		<td> Mute Microphone for : <input type="text" name="mtime" value="60"/> minutes </td>
		<td> <input name="Mute" type="submit" value="Mute"/> </td>
		</form>
                <td> <input type="button" name="unmute" value="Unmute" onclick="unmutecall()"/> </td>
		<td>
    			 <a href="./sound.php" target="_blank"><input type="button" value="Show Sound Input" /></a>
		</td>
		<td>
			<a href="./schedule.php" target="_blank"><input type="button" value="Open the Scheduler"/></a>
		</td>
        </tr>

</table>
</br>

<table id="file-controls" width="50%">
        <tr>
                <td> <h2> Local File Storage </h2> </td>
        </tr>
        <tr>
                <td>
                        <a href="./arecordings/" target="_blank"><input type="button" value="Show Local Recordings"/></a>
                </td>
        </tr>

</table>
</br>

<h1> Current Config.sh File </h1>
<?php
        if($_POST['Submit'])
        {
                $open = fopen("/home/pi/scripts/config.sh","w+");
                $text = $_POST['update'];
                $text_eol = str_replace("\r", "", $text);
                fwrite($open, $text_eol);
                fclose($open);
                echo "<h1>File updated.</h1><br />";
        }
?>

<table id="config-wrapper">
	<tr>
    	<td id="config-file">

	<?php
	$file = file("/home/pi/scripts/config.sh");
	echo "<form action=\"".$PHP_SELF."\" method=\"post\">";
	echo "<textarea id=\"configfile\" Name=\"update\" cols=\"100\" rows=\"40\">";

	foreach($file as $text) {
		echo $text;
	}

	echo "</textarea>";
	echo "</br>";
	echo "<input name=\"Submit\" type=\"submit\" value=\"Update\" />\n</form>";
	?>

    	</td>

    	<td id="config-edit">
        <?php
        $parameters = array();
        $output = shell_exec('cat /home/pi/scripts/config.sh');

        foreach(preg_split("/((\r?\n)|(\r\n?))/", $output) as $line){
                // do stuff with $line
                preg_match('/^([^#].+?)="?(.+?)"?$/m', $line, $matches);
                if (count($matches) > 1) {
                        //echo "<p> $matches[1] : $matches[2] </p>";
                        $parameter = array($matches[1], $matches[2]);
                        array_push($parameters, $parameter);
                }
        }
        //print_r($parameters);
	?>

	<form name='form' method='post' action="update_config.php">
	<?php
        	echo "<table>";
        	foreach($parameters as $parameter) {
                	echo "<tr>";
                	echo "<td>$parameter[0]:</td>";
                	echo '<td><input type="text" name="'.$parameter[0].'" value="'.$parameter[1].'" style="width: 400px;"></td>';
                	echo "</tr>";
        	}
        	echo "</table>";
	?>

	<input type="submit" name="submit" value="Update Config">

	</form>
    	</td>
	</tr>
</table>


<?php
echo "<h1> Main  Log: </h1>";
$output = shell_exec('tail -100 /home/pi/logs/log.log');
echo "<textarea cols=\"200\" rows=\"25\">$output</textarea>";
echo "<h1> LiquidSoap Log: </h1>";
$output = shell_exec('tail -100 /home/pi/logs/liquidsoap.log');
echo "<textarea cols=\"200\" rows=\"25\">$output</textarea>";
?>

</body>
