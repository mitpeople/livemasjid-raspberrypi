<html>
<head>
<script src="./js/jquery-3.1.0.js"></script>
<script>
function togglecell(e) {
	var background = e.style.backgroundColor;
	//console.log(background);
	if (background == 'red') {
        	e.style.backgroundColor = '';
	} else {
		e.style.backgroundColor = 'red';
    	}
	$('input[name=Save]')[0].disabled=false;
}

function togglecolumn(e) {
	var background = $('table tr td:nth-child('+(e.cellIndex+1)+')')[1].style.backgroundColor;
	if (background == 'red') {
		$('table tr td:nth-child('+(e.cellIndex+1)+')').css('background-color', '');
	} else {
		$('table tr td:nth-child('+(e.cellIndex+1)+')').css('background-color', 'red');
	}
	$('table tr:eq(0) td').css('background-color', '');
	$('input[name=Save]')[0].disabled=false;
}

function togglerow(e) {
	var background =  $('table tr:eq('+e.parentNode.rowIndex+') td')[1].style.backgroundColor;

	//console.log(e.parentNode.rowIndex);
	if (background == 'red') {
		$('table tr:eq('+e.parentNode.rowIndex+') td').css('background-color', '');
	} else {
		$('table tr:eq('+e.parentNode.rowIndex+') td').css('background-color', 'red');
	}
	$('table tr td:nth-child(1)').css('background-color', '');
	$('input[name=Save]')[0].disabled=false;
}

function clearall() {
	$("td").each(function() {
		$(this).css('background-color', '');
	});
	$('input[name=Save]')[0].disabled=false;
}
</script>

<script>
var day = new Array();
var hour = new Array(24);
var json_schedule;
var something;
function read_schedule_from_file() {
	//console.log("here A");
	$.ajax({
                url: './get_schedule.php',
                cache: false,
                dataType: 'json',
                success: function(response) {
                	//console.log(response);
			json_schedule = response;
			day = new Array();
			$.each(json_schedule, function (intIndex, objDay) {
                		//console.log(intIndex);
                		hour = objDay.split(' ').map(Number);
                		day.push(hour.slice());
        		});
			update_table_display();
			$('input[name=Save]')[0].disabled=true;
		}
	});
}

function write_schedule() {
	 get_schedule_from_table();
	 $.ajax({
                type: 'POST',
		url: './write_schedule.php',
                cache: false,
                data: {day},
		dataType: 'json',
                success: function(response) {
                        //console.log(response);
                }
        });
	$('input[name=Save]')[0].disabled=true;

}

function get_schedule_from_table() {
	var count = 0;
	day = new Array();
	$('.sdata').each(function(){
    		var color = "";
    		color = $(this)[0].style.backgroundColor;
		//console.log(color);
    		if (color == 'red') {
			//console.log("here");
        		hour[count] = 1;
    		} else {
        		hour[count] = 0;
    		}
    		if (count === 23) {
        		count=-1;
        		day.push(hour.slice());
    		}
    		count++;
	});
}

function update_table_display() {
	var sday = 0;
	var shour = 0;
        $('.sdata').each(function(){
                $(this)[0].style.backgroundColor = ((day[sday][shour] == 1) ? 'red' : '');
                if (shour === 23) {
                        shour=-1;
                        sday++;
                }
                shour++;
        });
}

</script>

<script>

$( document ).ready(function() {
	console.log( "ready!" );
	read_schedule_from_file();
});
</script>

</head>

<body>
<h1> Scheduler Configuration </h1> 
<h3> Please mark the hours to disable streaming in red, by clicking in the cells to toggle state</h3>
<p> You can toggle a full hour or full day by clicking on the title blocks for the day or hour <p>
<table id="hourly" width="80%" border="1" style="cursor: pointer;">
	<tr>
		<td></td>
		<td onclick="togglecolumn(this)">00</td><td onclick="togglecolumn(this)">01</td><td onclick="togglecolumn(this)">02</td>
		<td onclick="togglecolumn(this)">03</td><td onclick="togglecolumn(this)">04</td><td onclick="togglecolumn(this)">05</td>
		<td onclick="togglecolumn(this)">06</td><td onclick="togglecolumn(this)">07</td><td onclick="togglecolumn(this)">08</td>
		<td onclick="togglecolumn(this)">09</td><td onclick="togglecolumn(this)">10</td><td onclick="togglecolumn(this)">11</td>
		<td onclick="togglecolumn(this)">12</td><td onclick="togglecolumn(this)">13</td><td onclick="togglecolumn(this)">14</td>
		<td onclick="togglecolumn(this)">15</td><td onclick="togglecolumn(this)">16</td><td onclick="togglecolumn(this)">17</td>
		<td onclick="togglecolumn(this)">18</td><td onclick="togglecolumn(this)">19</td><td onclick="togglecolumn(this)">20</td>
		<td onclick="togglecolumn(this)">21</td><td onclick="togglecolumn(this)">22</td><td onclick="togglecolumn(this)">23</td>
	</tr>
	<tr>
		<td onclick="togglerow(this)"> Monday </td>
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
	</tr>
	<tr>
                <td onclick="togglerow(this)"> Tuesday </td>
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
        </tr>
	<tr>
                <td onclick="togglerow(this)"> Wednesday </td>
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
        </tr>
	<tr>
                <td onclick="togglerow(this)"> Thursday </td>
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
        </tr>
	<tr>
                <td onclick="togglerow(this)"> Friday </td>
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
        </tr>
	<tr>
                <td onclick="togglerow(this)"> Saturday </td>
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
        </tr>
	<tr>
                <td onclick="togglerow(this)"> Sunday </td>
		<td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
                <td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata"><td onclick="togglecell(this)" class="sdata">
        </tr>
</table>

</br>

<input name="Reset" type="button" value="Reset Changes" onclick="read_schedule_from_file()"/>
<input name="Clear All" type="button" value="Clear All" onclick="clearall()"/>
<input name="Save" type="button" value="Save" onclick="write_schedule()" disabled="disabled"/>

</body>
